/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.utils;

import org.eclipse.osbp.utils.js.constants.PathConstants;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public class JsAnnotation {

	public static final String getJsPath() {
		return PathConstants.COMMON_VAADIN_THEMES_PREFIX + PathConstants.COMMON_JS_PATH;
	}

	@SuppressWarnings("rawtypes")
	public static final String getJsPath(Class clazz) {
		Bundle bundle = FrameworkUtil.getBundle(clazz);
		String symbolicName = bundle.getSymbolicName();
		String pckgNamePath = clazz.getPackage().getName().replace(".", "/");
		return PathConstants.COMMON_VAADIN_THEMES_PREFIX + PathConstants.COMMON_PLUGIN_PREFIX + symbolicName + "/" + pckgNamePath + "/js/";
	}

}
